![Mazeterious 2.png](https://bitbucket.org/repo/gdaLgr/images/1232898617-Mazeterious%202.png)

*Mazeterious* is an open source, free to play stealth game developed by *Kanu* (see [Time2Gather.de](http://www.time2gather.de)). Its development is split into different *substeps* to be able to release often. The current, playable release will always be found on [Time2Gather.de/Mazeterious](http://www.time2gather.de/Mazeterious) (the quality and handling of the web player is pretty low, but it works). Problems or ideas may be reported as an [Issue](https://bitbucket.org/Time4Kanu/mazeterious/issues/new).

## Problems ##

*Mazeterious* is being developed in my spare time only. Thus the development speed is depending on the amount of time I'm able to put into it. Because I've got a full-time job and also need some time for my Let's Plays (see [Time2Gather.de](http://www.time2gather.de)), the project is growing a lot slower than I wish it would. Additionally, because *Mazeterious* currently is my own private project, it is my playground for new development techniques (like the Unity Engine itself or [Reactive Programming](http://reactivex.io)), slowing down the development speed a bit more. 

## Substeps ##

Each successfully implemented substep will add a bunch of functionality and may even change the goal, speed or theme of the game. The different substeps are arranged incrementally to be able to include suggestions, requests and test results from the players.

### Release 0.1: Simple Key Collection ([Done](http://time2gather.de/Mazeterious/v0.1)) ###

The first release is used for me to get used to the Unity Engine and to learn how to combine it with Reactive Programming. Therefore it has the goal to release a first, playable prototype of the game to build upon - not to include cool graphics, nice mechanics or even the final stealth theme of the game. 

* At the beginning of the game a maze is generated randomly. The number of cells (horizontally and vertically) as well as a certain seed define each maze unambiguously.
* The player spawns in a random cell in first person view and is able to move around freely, only blocked by the walls of the maze. He is able to use WASD to move, space to jump and shift to run. 
* The maze is completely dark with just a few randomly picked cells lit up by a ceiling light. The player has a flashlight revealing a small cone in front of him. He is able to toggle this flashlight by pressing F. 
* A certain amount of keys are spawned in different, randomly chosen cells within the maze. When the player gets close to them, they are lit up to be seen more easily. When the player touches a key, it gets collected. A UI element shows the number of keys collected until now as well as the whole number of keys to collect.
* When all keys have been collected, a door spawns anywhere in the maze. The player has to find that door and to touch it to win the game. After winning the game, the whole maze is recreated with a new seed.
* A UI element shows the number of seconds passed since the beginning of the game. When the game is won, the timer stops and shows the time needed for solving this maze to compare it with other players.

### Release 0.2: Configurable Maze ([Done](http://time2gather.de/Mazeterious/v0.2)) ###

The goal of this small substep is to give the player more control about the parameters of the maze creation. That way they can try more different sizes or configurations of the maze to give feedback on which maze configurations are more appropriate, which are too difficult and which are too easy. Additionally, players can now create individual, but identical mazes to compare their results in solving them.

* At the beginning of each game (also after winning the former game), the player is able to enter values for important parameters of the maze generation.

### Release 0.3: Introducing the Minotaurus ([Done](http://time2gather.de/Mazeterious/v0.3)) ###

Until now there might have been a way to win the game, but no way to loose. In this release, the goal is to introduce a hostile AI creating a threat to the player while he's collecting the keys. Because up to now the game is independent of a certain theme, this AI will get the generic name *enemy*. 

* A the beginning of the game, one or multiple enemies (depending on the maze's size) will spawn anywhere in the maze and will walk around randomly. 
* When the player touches an enemy, the game is lost. 
* Each enemy has a *sight* to be able to spot the player, maybe indicated by a flashlight. This sight has a certain range and is blocked by walls. When the player gets spotted, the game is lost.
* While the player uses his flashlight, the range restriction of the enemy's sight gets negated - because a light in the dark can be seen from way further away.

#### Release 0.3.1: No enemies ([Done](http://time2gather.de/Mazeterious/v0.3.1)) ####

A player requested that it also should be possible to explore the maze without any enemies, maybe to locate the keys beforehand. 

* It is possible to set up a game with 0 enemies.

### Release 0.4: Multiplayer (In Progress) ###

This big release will introduce a first multiplayer concept for the game. The players shall be able to team up and collect the keys together, but also have to organize themselves, so each one of them remains unseen until able to escape.

* Players can join each other's game.
* Collected keys count for all players.
* For winning, all players have to get to the door.
* The game is lost, if one of the players gets spotted.

### Release 0.5: Further development ###

I've got a lot of ideas for this game, but since I also want to include a lot of feedback (and *Your* ideas, too), I won't specify too many substeps a priori.
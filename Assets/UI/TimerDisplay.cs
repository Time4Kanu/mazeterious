﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Collections;

namespace Mazeterious {

	public class TimerDisplay : MonoBehaviour {


		/** The Timer to observe */
		public Timer Timer;

		[RangeReactiveProperty (0,10)]
		public IntReactiveProperty Precision = new IntReactiveProperty (2);

		/** The text to display the current timer */
		private Text Text;


		private void Start () {
			Text = GetComponent<Text> ();
			Timer.Seconds.Subscribe (UpdateText);
		}
		
		private void UpdateText (float value) {
			Text.text = "Time: " + value.RoundToPrecision (Precision.Value).ToString ();
		}

	}

}

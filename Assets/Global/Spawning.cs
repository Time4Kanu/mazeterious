﻿using UnityEngine;
using System.Collections;

public class Spawning : MonoBehaviour {

	public static Spawning StaticScript;

	private Spawning () {
		StaticScript = this;
	}

	public void DestroyGameObject(GameObject any) {
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.delayCall += () => DestroyImmediate (any);
		#else
		Destroy (any);
		#endif
	}

}

public static class SpawningExtensions {

	public static void RemoveFromEditor(this GameObject any) {
		foreach(Transform child in any.transform) {
			RemoveFromEditor (child.gameObject);
		}
		Spawning.StaticScript.DestroyGameObject (any);
	}

}

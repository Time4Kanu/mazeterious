﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {
	
    
	/** Toggles the pause menu */
	public InputCommand Pause = new InputCommand ();

	/** Toggles the flashlight (on/off) */
	public InputCommand FlashLight = new InputCommand ();


	/** A list of all possible commands to iterate over easily */
	private List<InputCommand> Commands = new List<InputCommand> ();

	/** Creates a new InputManager with given InputCommands as default */
	private InputManager () {
		Commands.Add (Pause);
		Commands.Add (FlashLight);	
	}


	/** Gets called each frame */
	private void Update () {
		if (Input.anyKey) {
			Commands.ForEach (command => command.CheckActivation ());
		} 
	}

    /** Called on the end of the scene */
    private void OnDestroy () {
        Commands.ForEach (command => command.Recreate ());
    }

}

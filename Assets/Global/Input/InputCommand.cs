﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;

/** A single Input Command (e.g. pressing a button) with the possibility to react to it */
[Serializable]
public class InputCommand {

	/** The primary KeyCode for this InputCommand */
	public KeyCode Primary;

	/** The secondary KeyCode for this InputCommand */
	public KeyCode Secondary;

	/** The event occuring when either the primary or the secondary key for this InputCommand gets pressed */
	public Subject<KeyCode> Trigger = new Subject<KeyCode> ();

	/** Checks, if this InputCommand should call its event */
	public void CheckActivation() {
		if (Input.GetKeyDown (Primary)) {
			Trigger.OnNext (Primary);
		} else if (Input.GetKeyDown (Secondary)) {
            Trigger.OnNext (Secondary);
		} 
	}

    /** Recreates the trigger of this InputCommand */
    public void Recreate () {
        Trigger.Dispose ();
        Trigger = new Subject<KeyCode> ();
    }

}

﻿using UnityEngine;
using System.Collections;

public class Metronome : MonoBehaviour {
	

	/** The amount of ticks each second */
	public static readonly int TICKS_PER_SECOND = 32; 

	/** The amount of time passed between each tick */
	public static readonly float SECONDS_PER_TICK = 1F / TICKS_PER_SECOND;

	/** The time elapsed since the last tick */
	private float TimeSinceLastTick = 0;

	/** Defines whether the Metronome is ticking */
	public bool Running = true;


	/** The event of a single tick */
	private static event Action<float> TickEvent;


	/** Allows to register a Ticker to the global tick event forever */
	public static void RegisterForever(Action<float> ticker) {
		TickEvent += ticker;
	}

	/** Allows to deregister a Ticker from the global tick event */
	public static void Deregister(Action<float> ticker) {
		TickEvent -= ticker;
	}

	/** Allows to register a Ticker to the global tick event for a certain time */
	public static TickerWithDuration Register(float durationInSeconds, Action<float> ticker) {
		return Register (durationInSeconds, ticker, null);
	}

	/** Allows to register a Ticker to the global tick event with a certain function called when the Ticker ends */
	public static TickerWithDuration Register(float durationInSeconds, Action onFinish) {
		return Register (durationInSeconds, null, onFinish);
	}

	/** Allows to register a Ticker to the global tick event for a certain time and with a 
	 * certain function called when the Ticker ends */
	public static TickerWithDuration Register(float durationInSeconds, Action<float> ticker, Action onFinish) {		
		return TickerWithDuration.Create(durationInSeconds, ticker, onFinish);
	}

	/** Allows to register an action only for one tick. It will be executed the next tick and removed afterwards. */
	public static TickerWithDuration RegisterForOneTick(Action action) {
		return Register (SECONDS_PER_TICK, null, action);
	}
			
	/** Manages the ticking mechanism **/
	void Update () {		
		if (Running) {
			TimeSinceLastTick += Time.deltaTime;

			while (TimeSinceLastTick >= SECONDS_PER_TICK) {
				TimeSinceLastTick -= SECONDS_PER_TICK;
				TickEvent.IfNotNull (e => e (SECONDS_PER_TICK));
			}
		}
	}


	/** Creates a Ticker, which lives a certain time and does something specific when finished */
	public class TickerWithDuration {

		private readonly int DurationInTicks;
		private readonly Optional<Action<float>> Ticker;
		private readonly Optional<Action> OnFinish;

		private int ticksPassed = 0;

		public static TickerWithDuration Create(float durationInSeconds, Action<float> ticker, Action onFinish) {
			int durationInTicks = Mathf.RoundToInt(durationInSeconds * TICKS_PER_SECOND);
			return new TickerWithDuration (durationInTicks, ticker, onFinish);
		}

		private TickerWithDuration(int durationInTicks, Action<float> ticker, Action onFinish) {
			this.DurationInTicks = durationInTicks;
			this.Ticker = ticker;
			this.OnFinish = onFinish;

			TickEvent += Tick;
		}

		private void Tick(float deltaTime) {
			Ticker.MaybeDo(f => f(deltaTime));
			ticksPassed++;

			if(ticksPassed >= DurationInTicks) {
				TickEvent -= Tick;
				OnFinish.MaybeDo (f => f());
			}
		}

		public void Interrupt() {
			TickEvent -= Tick;
		}

	}
}
﻿using UnityEngine;
using UniRx;
using System.Collections;

namespace Mazeterious {

	public class PauseMenu : MonoBehaviour {

        /** The InputManager giving access to the Pause Trigger */
        public InputManager Input;

		/** The Menu to be shown */
		public GameObject Menu;


		/** Initializes this PauseMenu */
		private void Start () {
			Menu.SetActive (false);
			Input.Pause.Trigger.Subscribe (_ => TriggerPauseMenu ()).AddTo (gameObject);
		}

		/** Triggers the pause menu */
		public void TriggerPauseMenu () {
			Menu.SetActive (!Menu.activeSelf);
			ShowCursor (Menu.activeSelf);
			Time.timeScale = Menu.activeSelf ? 0 : 1;
		}

		/** Shows or hide the cursor */
		private void ShowCursor (bool show) {
			Cursor.visible = show;
			Cursor.lockState = show ? CursorLockMode.None : CursorLockMode.Locked;
		}

        /** Quits this scene and goes back to the main menu */
		public void Quit () {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
		}

	}

}
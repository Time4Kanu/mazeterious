﻿using UnityEngine;
using UniRx;
using System.Collections;

namespace Mazeterious {

	public class Timer : MonoBehaviour {


		/** The seconds passed since the start of the game */
		public FloatReactiveProperty Seconds = new FloatReactiveProperty (0);

						
		private void Update () {
			Seconds.Value = Seconds.Value + Time.deltaTime;
		}

	}

}
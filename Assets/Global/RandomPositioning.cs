﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	public class RandomPositioning : MonoBehaviour {

		public Seeder Seed;

		private void Start () {
			var maze = GameObject.Find ("Maze").GetComponent<Maze> ();
			maze.Positioner.RandomlyPositionPlayer (Seed, transform);
		}

	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Collections;

namespace Mazeterious {

	[RequireComponent(typeof(Metronome))]
	public class GameLogic : MonoBehaviour {


		/** Teh Display to be activated when the game is won */
		public Text WinningDisplay;

		/** The text to be shown when loosing the game */
		public Text LoosingDisplay;

		/** The text to display the current timer */
		public GameObject TimerDisplay;

		/** The Timer to get the time to show from */
		public Timer Timer;

		/** The door triggering the Winning event */
		public Door Door;

		/** The enemy to trigger the loosing event */
		public Enemies AllEnemies;

		/** The time (in seconds) to stay in the winning screen */
		public FloatReactiveProperty WinningScreenTime = new FloatReactiveProperty (3);
        

		/** Gets called at the beginning of the game */
		private void Start () {
			Time.timeScale = 1;
			Door.DoorTouched.Subscribe (WinGame);
            ResubscribeToPlayerDetection (Unit.Default);
            AllEnemies.EnemiesRespawned.Subscribe (ResubscribeToPlayerDetection);
		}

        /** Renews the subscription to all eisting enemy detection scripts */
        private void ResubscribeToPlayerDetection<T> (T nil) {
            AllEnemies.AllEnemies.ForEach (enemy => enemy.Detection.PlayerSpotted.Subscribe (LooseGame));
        }

		/** To be called when winning the game */
		public void WinGame<T> (T nil) {
			EnsureValidObjects ();

			Time.timeScale = 0F;

			TimerDisplay.SetActive (false);
			WinningDisplay.gameObject.SetActive (true);
			WinningDisplay.text = "You Won!\nTime: " + Timer.Seconds.Value.RoundToPrecision (2);

			StartCoroutine (Quit ());
		}

		/** After recreating the scene, some of the links might be broken and need to be reinitialized */
		private void EnsureValidObjects () {
			TimerDisplay = GameObject.Find ("Canvas").transform.Find (TimerDisplay.name).gameObject;
			WinningDisplay = GameObject.Find ("Canvas").transform.Find (WinningDisplay.name).GetComponent<Text> ();
		}

		/** To be called when loosing the game */
		public void LooseGame<T> (T nil) {
			EnsureValidObjects ();

			Time.timeScale = 0F;

			TimerDisplay.SetActive (false);
			LoosingDisplay.gameObject.SetActive (true);
			LoosingDisplay.text = "You loose!\nTime: " + Timer.Seconds.Value.RoundToPrecision (2);

			StartCoroutine (Quit ());
		}

		/** Restarts the game anew */
		public IEnumerator Quit () {
			yield return new WaitForSecondsRealtime (WinningScreenTime.Value);
			UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
		}

	}

}
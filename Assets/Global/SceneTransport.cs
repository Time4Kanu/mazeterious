﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	public static class SceneTransport {
		
		/** A static variable to transport a seed value between scenes */
		public static int? Seed;

		/** A static variable to transport the number of rows between scenes */
		public static int? Rows;
         
		/** A static variable to transport the number of columns between scenes */
		public static int? Columns;

		/** A static variable to transport the number of keys between scenes */
		public static int? NumberOfKeys;

        /** A static variable to transport the number of enemies between scenes */
        public static int? NumberOfEnemies;

	}

}
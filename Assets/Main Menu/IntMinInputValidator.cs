﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Mazeterious {

	[RequireComponent (typeof (InputField))]
	public class IntMinInputValidator : MonoBehaviour {

		private InputField ToSecure;

		public int Min = 1;

		private void Start () {
			ToSecure = GetComponent<InputField> ();
			ToSecure.onEndEdit.AddListener (EnsureGreaterZero);
		}

		public void EnsureGreaterZero (string newValue) {
			if (newValue.Length == 0 || int.Parse (newValue) <= Min) {
				ToSecure.text = Min.ToString ();
			}
		}


	}

}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Mazeterious {

	public class MainMenu : MonoBehaviour {

	
		/** The seed to create the maze */
		public InputField Seed;

		/** The number of rows of the maze to create */
		public InputField Rows;

		/** The number of rows of the maze to create */
		public InputField Columns;

		/** The number of keys to spawn in the maze */
		public InputField NumberOfKeys;

        /** The number of enemies to spawn in the maze */
        public InputField NumberOfEnemies;


		/** Called when loading the main menu */
		private void Start () {
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
			LoadStaticValues ();
		}

		/** Loads the stored static values, if any */
		private void LoadStaticValues () {
			if (SceneTransport.Seed.HasValue) {
				Seed.text = SceneTransport.Seed.ToString ();
				Rows.text = SceneTransport.Rows.ToString ();
				Columns.text = SceneTransport.Columns.ToString ();
				NumberOfKeys.text = SceneTransport.NumberOfKeys.ToString ();
                NumberOfEnemies.text = SceneTransport.NumberOfEnemies.ToString ();
			}
		}


		/** Starts a new game with the given configuration */
		public void StartNewGame () {
			SaveToStaticValues ();
			UnityEngine.SceneManagement.SceneManager.LoadScene ("Maze");
		}

		/** Saves the chosen configuration to static values to pass them to the next scene */
		private void SaveToStaticValues () {
			SceneTransport.Seed = int.Parse (Seed.text);
			SceneTransport.Rows = int.Parse (Rows.text);
			SceneTransport.Columns = int.Parse (Columns.text);
			SceneTransport.NumberOfKeys = int.Parse (NumberOfKeys.text);
            SceneTransport.NumberOfEnemies = int.Parse (NumberOfEnemies.text);
		}

	}

}
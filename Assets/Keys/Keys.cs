﻿using UnityEngine;
using UniRx;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	public class Keys : MonoBehaviour {

		[System.Serializable]
		public class Config {

			/** The Gameobject to spawn the keys into */
			public KeyHolder KeyHolder; 

		}


		/** The total amount of keys in this Maze */
		[RangeReactiveProperty(1,30)]
		public IntReactiveProperty AmountOfKeys = new IntReactiveProperty (1);


		/** The Spawner of all keys */
		public KeySpawner Spawner;

		/** The logic to collect all keys */
		public KeyCollector Collector;

		/** The configuration of these keys */
		public Config Configuration = new Config ();


        /** Creates a new Keys script */
		public Keys () {
			Spawner = new KeySpawner (this);
			Collector = new KeyCollector (this);
		}

        /** Called at the beginning of the game */
		private void Start () {
			AmountOfKeys.Value = SceneTransport.NumberOfKeys ?? AmountOfKeys.Value;
			Collector.Start ();
		}

	}

}

﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	[System.Serializable]
	public class KeyRotation {


		/** The key mesh to rotate */
		public Transform KeyMesh;

		/** The speed of the rotation in angles per tick */
		public float Speed = 5F;


		/** Called once per frame */
		public void Rotate () {
			KeyMesh.Rotate (Vector3.up, Speed * Time.deltaTime, Space.World);
		}

	}

}
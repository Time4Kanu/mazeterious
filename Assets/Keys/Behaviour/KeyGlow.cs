﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	[System.Serializable]
	public class KeyGlow {


		/** The key to activate the glow on */
		public Transform Key;

		/** The glow effect to activate */
		public GameObject Glow;

		/** The detection range of the  */
		public float DetectionRange = 10;


		/** The player activating the glow */
		private Transform Player;

		public void Start () {
			Player = GameObject.FindWithTag ("Player").transform;
		}

		/** Checks, if activating the glow is nesseccary */
		public void CheckGlow () {
			if (Player && Key) {
				float distance = (Player.position - Key.position).magnitude;
				Glow.SetActive (distance <= DetectionRange);
			}
		}

	}

}
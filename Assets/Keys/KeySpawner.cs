﻿using UnityEngine;
using UniRx;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	[Serializable]
	public class KeySpawner {


		/** The maze to spawn the keys in */
		public Maze Maze;

		/** The prefab for keys to instantiate */
		public Key KeyPrefab;


		/** The Keys object containing the amount of keys to spawn */
		private Keys Keys;

		public KeySpawner (Keys keys) {
			Keys = keys;
			Keys.AmountOfKeys.SkipLatestValueOnSubscribe ().Subscribe (RespawnKeys);
			Maze.Recreation.Subscribe (RespawnKeys); 			
		}


		/** Respawns all nesseccary keys */
		private void RespawnKeys<T> (T nil) {
			EnsureValidObjects ();
			RemoveAllKeys ();
			Keys.AmountOfKeys.Value.Times (SpawnKey);
			Maze.Positioner.RandomlyPositionActors (Maze.Seeder, 
				Keys.Configuration.KeyHolder.AllKeys.Select (key => key.transform));
		}

        /** Ensures, that all used object references are still valid */
		private void EnsureValidObjects () {
			Maze = GameObject.Find ("Maze").GetComponent<Maze> ();
			Keys = GameObject.Find ("Keys").GetComponent<Keys> ();
		}

		/** Removes all Keys associated to this Keys object */
		private void RemoveAllKeys () {
			Keys.Configuration.KeyHolder.AllKeys.Clear ();
			foreach (Transform key in Keys.Configuration.KeyHolder.transform) {
				key.gameObject.RemoveFromEditor ();
			}
		}

		/** Create a new key on the map, randomly positioned */
		private void SpawnKey (int number) {
			Key key = (Key) GameObject.Instantiate (KeyPrefab, 
				KeyPrefab.transform.position, Quaternion.identity, Keys.Configuration.KeyHolder.transform);
			key.Initalize (number);
			Keys.Configuration.KeyHolder.AllKeys.Add (key);
		}

	}

}
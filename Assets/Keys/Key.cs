﻿using UnityEngine;
using System.Collections;


namespace Mazeterious {

	public class Key : MonoBehaviour {


		/** The glowing effect of the key */
		public KeyGlow Glow;

		/** The permanent rotation of the key */
		public KeyRotation Rotation;

		/** Makes this key collectable */
		public KeyCollectable Collectable;


		/** Initializes this Key */
		public void Initalize (int number) {
			name = "Key " + number;
		}

        /** Called at the beginning of the game */
		private void Start () {
			Glow.Start ();
		}

		/** Called each frame */
		private void Update () {
			Glow.CheckGlow ();
			Rotation.Rotate ();
		}

		/** Called when something enters this trigger collider */
		private void OnTriggerEnter(Collider coll) {
			Collectable.OnTriggerEnter (coll);
		}

	}

}

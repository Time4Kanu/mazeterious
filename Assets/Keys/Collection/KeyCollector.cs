﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	[System.Serializable]
	public class KeyCollector {


		/** The observable amount of collected keys up to now */
		public IntReactiveProperty AmountOfCollectedKeys = new IntReactiveProperty (0);

		/** An event happening once all keys have been collected */
		public Subject<Unit> AllKeysCollected = new Subject<Unit> ();


		/** The Maze containing the KeySpawner */
		private readonly Keys Keys;

		/** Called at the beginning of the game */
		public KeyCollector (Keys keys) {
			Keys = keys;
		}

		public void Start () {
			Keys.Configuration.KeyHolder.AllKeys.ForEach (key => key.Collectable.Collected.Subscribe (CollectKey));		
		}


		/** Called when a single key is collected */
		private void CollectKey (KeyCollectable key) {
			AmountOfCollectedKeys.Value = AmountOfCollectedKeys.Value + 1;
			if (AmountOfCollectedKeys.Value >= Keys.AmountOfKeys.Value) {
				AllKeysCollected.OnNext (Unit.Default);
			}
		}

	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Collections;

namespace Mazeterious {

	public class KeyCollectorDisplay : MonoBehaviour {


		/** The Text to show the currently collected amount of keys */
		private Text Text;

		/** The Keys object to show the currently collected keys */
		public Keys Keys;


		/** Called at the beginning of the game */
		private void Start () {
			Text = GetComponent<Text> ();
			Keys.AmountOfKeys.Subscribe (_ => UpdateText (0));
			Keys.Collector.AmountOfCollectedKeys.Subscribe (UpdateText);
			Keys.Collector.AllKeysCollected.Subscribe(RemoveText);
			UpdateText (0);
		}


		/** Updates the Text to show the current amount of collected keys */
		void UpdateText (int collectedKeys) {
			Text.text = string.Format ("Keys: {0}/{1}", collectedKeys, Keys.AmountOfKeys.Value);
		}
			
		void RemoveText<T> (T nil) {
			Text.gameObject.SetActive (false);
			this.gameObject.SetActive (false);
		}

	}

}
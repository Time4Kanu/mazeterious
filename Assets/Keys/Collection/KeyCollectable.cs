﻿using UnityEngine;
using UniRx;
using System.Collections;


namespace Mazeterious {

	[System.Serializable]
	public class KeyCollectable {


		/** The key to be collectible */
		public GameObject Key;

		/** The event occuring, when a key is collected */
		public Subject<KeyCollectable> Collected = new Subject<KeyCollectable> ();


		private bool HasBeenCollectedYet = false;

		/** Called when something enters this trigger collider */
		public void OnTriggerEnter(Collider coll) {
			if (!HasBeenCollectedYet && coll.gameObject.CompareTag ("Player")) {
				HasBeenCollectedYet = true;
				Collected.OnNext (this);
				Key.SetActive (false);
			}
		}

	}

}
﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Mazeterious {

	[Serializable]
	public class Seeder {
		
		/** The global seed to be used to override the other ones */
		public IntReactiveProperty SeedProperty = new IntReactiveProperty (0);
		public Seed Seed { 
			get { return SeedProperty.AsSeed (); } 
			set { SeedProperty.Value = value.Value; }
		}

		/** Creates a new seed based on the current seed */
		public Seed RenewSeed () {
			Seed = Seed.RandomSeed ();
			return Seed;
		}


		/** Implicit translation into an IntReactiveProperty */
		public static implicit operator IntReactiveProperty (Seeder seeder) {
			return seeder.SeedProperty;
		}

		/** Implicit translation into a Seed */
		public static implicit operator Seed (Seeder seeder) {
			return seeder.Seed;
		}

	}

}

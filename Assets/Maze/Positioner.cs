﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Mazeterious {

	public class MazePositioner {
		

		/** The Maze this Positioner belongs to */
		private readonly MazeCellGrid Cells;


		/** Creates a new MazePositioner for a given Maze */
		public MazePositioner (MazeCellGrid cells) {
			Cells = cells;
		}

		/** Returns a random cell of this maze */
		public MazeCell RandomMazeCell (Seed seed) {
			return Cells.RandomElement (seed);
		}

		/** Positions the given actors randomly in this Maze's cells */
		public void RandomlyPositionActors (Seed seed, IEnumerable<Transform> actors) {
			actors.Zip (Cells.Shuffle (seed), MoveActorToCell);
		}

		/** Positions a given actor randomly in any of this Maze's cells */
		public MazeCell RandomlyPositionActor (Seed seed, Transform actor) {	
			return RandomlyPositionActor (seed, actor, Cells);
		}

		/** Positions a given actor randomly in any of the given cells */
		private static MazeCell RandomlyPositionActor(Seed seed, Transform actor, IEnumerable<MazeCell> possibleCells) {
			MazeCell actorCell = possibleCells.RandomElement (seed);
			MoveActorToCell (actor, actorCell);
			return actorCell;
		}

		/** Positions the given door randomly in this Maze */
		public void RandomlyPositionDoor (Seed seed, Transform door) {
			var doorCells = Cells.Where (cell => cell.Shape () != MazeCellShape.Crossroad);
			MazeCell doorCell = RandomlyPositionActor (seed, door, doorCells);
			RotateActorTowards (seed, door, doorCell.Walls.RandomWall (seed).transform);
		}

		public void RandomlyPositionPlayer (Seed seed, Transform player) {
			var playerCells = Cells.Where (cell => cell.Shape () == MazeCellShape.DeadEnd);
			var playerCell = RandomlyPositionActor (seed, player, playerCells);
			var lookAt = playerCell.Location.ReachableCells.RandomElement (seed);
			RotateActorTowards (seed, player, lookAt.transform);
		}

		/** Changes the given actor's position to the given cell's */
		private static void MoveActorToCell (Transform actor, MazeCell cell) {
			actor.position = cell.transform.position.WithY (actor.position.y);
		}

		public static void RotateActorTowards (Seed seed, Transform actor, Transform target) {
			actor.rotation = Quaternion.LookRotation ((target.position - actor.position).WithY (0));
		}

	}

}

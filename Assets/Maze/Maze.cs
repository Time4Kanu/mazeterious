﻿using UnityEngine;
using UniRx;
using System.Collections;
using Pathfinding;

namespace Mazeterious {


	public class Maze : MonoBehaviour {


		/** The seed to create this maze */
		public Seeder Seeder = new Seeder ();

		/** All possible Indices for this Maze */
		public MazeDimension Dimension = new MazeDimension ();

		/** The MazeCells object containing all cells of this maze */
		public MazeCellGrid Cells;

		/** The lighting effects for this maze */
		public MazeLighting Lighting;

		/** Allows position strategies for actors of this Maze */
		public MazePositioner Positioner;

		/** Allows to locate actors in this maze */
		public MazeLocator Locator;


		/** The event of completing a recreation of this maze */
		public static Subject<Maze> Recreation = new Subject<Maze> ();

		 
		/** Create a new Maze script */  
		private Maze () {  
			Cells = new MazeCellGrid (Dimension);
			Lighting = new MazeLighting (this);
			Positioner = new MazePositioner (Cells);
			Locator = new MazeLocator (this);

			Dimension.PossibleIndices.SkipLatestValueOnSubscribe ().Subscribe (RecreateMaze); 
			Seeder.SeedProperty.SkipLatestValueOnSubscribe ().Subscribe (RecreateMaze);
		}

		/** Called when starting the game */
		private void Start () {	
			Seeder.SeedProperty.Value = SceneTransport.Seed ?? Seeder.SeedProperty.Value;
			Dimension.Rows.Value = SceneTransport.Rows ?? Dimension.Rows.Value;
			Dimension.Columns.Value = SceneTransport.Columns ?? Dimension.Columns.Value;
			Locator.Start ();
		}


		/** Recreates the whole maze based on the current config values */
		public void RecreateMaze<T> (T nil) {
			Cells.Recreate ();
			this.GenerateMaze (Seeder, Dimension.Columns.Value, Dimension.Rows.Value);
			if (Cells.Configuration.CellGrid && Recreation != null) {
				Recreation.OnNext (this);
			}
		}

		/** Called when destroying this maze */
		private void OnDestroy () {
			Recreation.Dispose ();
			Recreation = new Subject<Maze> ();
		}

	}

}
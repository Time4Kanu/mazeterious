﻿using UnityEngine;
using System.Linq;
using UniRx;
using System.Collections;
using System.Collections.Generic;

namespace Mazeterious {

	[System.Serializable]
	public class MazeDimension {
	
		
		/** The amount of rows of cells in this maze  */
		public IntReactiveProperty Rows = new IntReactiveProperty (1);

		/** The amount of columns of cells in this maze */
		public IntReactiveProperty Columns = new IntReactiveProperty (1);

		/** All possible Indices for this maze based on the given Rows and Columns */
		public IndexListReactiveProperty PossibleIndices = new IndexListReactiveProperty ();

		   
		/** Creates a new Maze Dimension */
		public MazeDimension () { 
			Rows.Subscribe (newValue => Rows.Value = Mathf.Max(1, newValue));
			Rows.Where (Primitives.IsPositive).DistinctUntilChanged ().Subscribe (UpdatePossibleIndices);

			Columns.Subscribe (newValue => Columns.Value = Mathf.Max(1, newValue));
			Columns.Where (Primitives.IsPositive).DistinctUntilChanged ().Subscribe (UpdatePossibleIndices);			
		} 

		/** Updates the Possible Indices based on the current values of Rows and Column */
		private void UpdatePossibleIndices (int nil) {
			PossibleIndices.Value = Columns.Value.Times (column => Rows.Value.Times (row => 
				new Index (column - 1, row - 1))).Merge ().ToList ();
		} 

	}
	 
}

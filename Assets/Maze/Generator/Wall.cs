﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious._MazeGenerator {

	public abstract class Wall  {

		protected readonly Index Coords;

		protected Wall(Index coords) {
			this.Coords = coords;
		}

		public abstract Pair<Cell, Cell> SeparatedCells (Dictionary<Index, Cell> cells);

	}

	public class VerticalWall : Wall {

		public VerticalWall (Index coords) : base(coords) { }

		public override Pair<Cell, Cell> SeparatedCells (Dictionary<Index, Cell> cells) {
			Cell left = cells[Coords];
			Cell right = cells[Coords.PlusX(1)];
			return new Pair<Cell, Cell> (left, right);
		}

	}

	public class HorizontalWall : Wall {

		public HorizontalWall (Index coords) : base(coords) { }

		public override Pair<Cell, Cell> SeparatedCells (Dictionary<Index, Cell> cells) {
			Cell top = cells[Coords];
			Cell down = cells[Coords.PlusY(1)];
			return new Pair<Cell, Cell> (top, down);
		}

	}

}
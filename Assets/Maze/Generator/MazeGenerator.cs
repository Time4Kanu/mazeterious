﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mazeterious._MazeGenerator;

namespace Mazeterious {

	public static class MazeGenerator {

		public static void GenerateMaze(this Maze result, Seed seed, int columns, int rows) {
			var walls = GenerateWalls (columns, rows, seed);
			var cells = AllPossibleCells (result, columns, rows);

			foreach(Wall wall in walls) {
				var separatedCells = wall.SeparatedCells(cells);
				if (!separatedCells.a.IsConnectedTo (separatedCells.b)) {
					separatedCells.a.ConnectTo (separatedCells.b);
				} 
			}
		}

		private static List<Wall> GenerateWalls(int columns, int rows, Seed seed) {
			return new List<Wall> ().AddVerticalWalls (columns, rows)
									.AddHorizontalWalls (columns, rows)
									.Shuffle(seed);
		}

		private static List<Wall> AddVerticalWalls (this List<Wall> walls, int columns, int rows) {
			for (int col = 0; col < columns - 1; col++) {
				for (int row = 0; row < rows; row++) {
					walls.Add (new VerticalWall (new Index (col, row)));
				}
			}
			return walls;
		}

		private static List<Wall> AddHorizontalWalls (this List<Wall> walls, int columns, int rows) {
			for (int col = 0; col < columns; col++) {
				for (int row = 0; row < rows - 1; row++) {
					walls.Add (new HorizontalWall (new Index (col, row)));
				}
			}
			return walls;
		}

		private static Dictionary<Index, Cell> AllPossibleCells(Maze maze, int columns, int rows) {
			var cells = new Dictionary<Index, Cell> ();
			for (int col = 0; col < columns; col++) {
				for (int row = 0; row < rows; row++) {
					Index index = new Index (col, row);
					cells.Add (index, new Cell(maze.Cells.Cells[index]));
				}
			}
			return cells;
		}
	}

}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious._MazeGenerator {

	public class Cell {

		private HashSet<Cell> Room = new HashSet<Cell>();

		private readonly MazeCell MazeCell;

		public Cell(MazeCell cell) {
			Room.Add (this);
			MazeCell = cell;
		}

		public void ConnectTo(Cell other) {
			var newRoom = new HashSet<Cell> ();
			newRoom.UnionWith (Room);
			newRoom.UnionWith (other.Room);

			foreach (Cell cell in newRoom) {
				cell.Room = newRoom;
			}

			MazeCell.Walls.RemoveWallTo (other.MazeCell);
		}

		public bool IsConnectedTo(Cell other) {
			return other.Room == this.Room;
		}

	}

}
﻿using UnityEngine;
using UniRx;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	[Serializable]
	public class MazeLighting {


		/** The percentage of Passages, which's top lights shall become activated */
		[RangeReactiveProperty(0,100)]
		public FloatReactiveProperty TopLightPercentage = new FloatReactiveProperty (50F);


		/** Creates a new MazeLighting for this maze */
		public MazeLighting (Maze maze) {
			TopLightPercentage.SkipLatestValueOnSubscribe ()
				.Subscribe (_ => RandomlyActivateTopLights (maze.Seeder));
			Maze.Recreation.Subscribe (_ => RandomlyActivateTopLights (maze.Seeder));
		} 


		/** Chooses some passages randomly, which shall activate their top lights */
		private void RandomlyActivateTopLights (Seeder seeder) {
			var cells = GameObject.Find ("Maze").GetComponent<Maze> ().Cells;
			cells.ForEach (cell => cell.SetTopLight (false));
			IEnumerable<MazeCell> passages = cells.Where (cell => cell.Shape () == MazeCellShape.Passage);
			int amount = Mathf.RoundToInt (passages.Count () * TopLightPercentage.Value / 100F);
			passages.Shuffle (seeder).Take (amount).ForEach (cell => cell.SetTopLight (true));
		}

	}

}
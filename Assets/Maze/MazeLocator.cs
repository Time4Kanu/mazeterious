﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	[System.Serializable]
	public class MazeLocator {

		/** The cells to locate something in */
		private Maze Maze;

		/** The position of the maze in world coordinates */
		private Vector3 MazePosition;

		/** The dimension of each single, square shaped cell */
		private float CellDimension;


		/** Initialize this MazeLocator */
		public MazeLocator (Maze maze) {
			Maze = maze;
		}

		public void Start () {			
			MazePosition = Maze.gameObject.transform.position;
			Debug.Assert (Maze.Cells.Configuration.CellPrefab.transform.localScale.x 
				== Maze.Cells.Configuration.CellPrefab.transform.localScale.z, 
				"Prefab has to be square shaped");
			CellDimension = Maze.Cells.Configuration.CellPrefab.transform.localScale.x;
		}

		/** Returns the cell covering the given position */
		public MazeCell GetCellByPosition(Vector3 position) {
			Vector3 distance = (position - MazePosition) / CellDimension;
			return Maze.Cells.Cells[new Index (Mathf.RoundToInt (distance.x), Mathf.RoundToInt (distance.z))];
		}

	}

}
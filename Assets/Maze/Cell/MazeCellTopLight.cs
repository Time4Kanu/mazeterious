﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	public class MazeCellTopLight : MonoBehaviour {

		/** Called when a player is inside the lit area */
		private void OnTriggerStay (Collider collider) {
			if (collider.tag.Equals ("Player")) {
				collider.GetComponent<Player> ().IsLit = true;
			}
		}

		/** Called when a player leaves the lit area */
		private void OnTriggerExit (Collider collider) {
			if (collider.tag.Equals ("Player")) {
				collider.GetComponent<Player> ().IsLit = false;
			}
		}

	}

}
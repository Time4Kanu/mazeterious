﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	[Serializable]
	public class MazeCellLocation {
		

		/** The index of this cell inside the grid **/
		public Index Index;

		/** Returns all cells accessible through these walls */
		public List<MazeCell> ReachableCells = new List<MazeCell> ();

		/** Returns all 4 cells neighbouring this cell */
		public List<MazeCell> NeighbouringCells = new List<MazeCell> ();


		public MazeCellLocation(Index index) {
			Index = index;
		}


		/** Resets the reachable and neighbouring cells of this cell */
		public void Reset() {		
			ReachableCells.Clear ();
			NeighbouringCells.Clear ();
		}

		/** Adds all adjacent cells to the cell at the given index */
		public void AddNeighbours(MazeCellGrid cells, MazeDimension dimension) {
			if(Index.X > 0) {
				NeighbouringCells.Add (cells.Cells[Index.PlusX (-1)]);
			}
			if(Index.Y > 0) {
				NeighbouringCells.Add (cells.Cells[Index.PlusY (-1)]);
			}
			if(Index.X < dimension.Columns.Value - 1) {
				NeighbouringCells.Add (cells.Cells[Index.PlusX (1)]);
			}
			if(Index.Y < dimension.Rows.Value - 1) {
				NeighbouringCells.Add (cells.Cells[Index.PlusY (1)]);
			}
		}

	}

}
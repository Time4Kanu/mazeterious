﻿using UnityEngine;
using UniRx;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	public class MazeCell : MonoBehaviour {

		[System.Serializable]
		public class Config {

			/** The ceiling of this maze cell */
			public GameObject Ceiling;

			/** The top light of this cell */
			public GameObject TopLight;

		}


		/** The walls of this cell */
		public MazeCellWalls Walls;

		/** The location of this cell inside the maze */
		public MazeCellLocation Location;

		/** The configuration of this MazeCell */
		public Config Configuration = new Config ();


		public MazeCell () {
			Walls = new MazeCellWalls (this);
		}


		/** Called at the start of the game */
		private void Start () {
			SetCeiling (true);
		}


		/** Initializes this cell with the given attributes */
		public void Initialize (Index index, bool ceiling) {
			gameObject.name = string.Format ("Cell {0}", index.ToString ());
			SetCeiling (ceiling);
			Location = new MazeCellLocation(index);
			Walls.Reset ();
		}


		/** Opens or closes the cell's ceiling */
		public void SetCeiling(bool ceiling) {
			Configuration.Ceiling.SetActive (ceiling);
		}

		/** (De-)activates the top light of this cell */
		public void SetTopLight (bool active) {
			Configuration.TopLight.SetActive (active);
		}

	}

}

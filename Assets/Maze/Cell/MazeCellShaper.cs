using UnityEngine;
using System;
using System.Collections;


namespace Mazeterious {

	/** Evaluates the shape of a maze cell */
	public static class MazeCellShaper {

		/** Returns the shape of this cell when looked on it from above */
		public static MazeCellShape Shape(this MazeCell cell) {

			/** Check, how many cells are reachable from this one */
			switch (cell.Location.ReachableCells.Count) {

			/** Zero reachable cells means there is no way nowhere */
			case 0:
				return MazeCellShape.Trap;

			/** A cell with just one reachable cell is a dead-end */
			case 1:
				return MazeCellShape.DeadEnd;

			/** If a cell has two reachable cells, it can be either a passage or a corner */
			case 2:
				return CanBeTraversedStraight(cell.Walls) ? MazeCellShape.Passage : MazeCellShape.Corner;

			/** All other cells must be crossroads */
			default: 
				return MazeCellShape.Crossroad;
			}
		}

		/** Checks, whether opposite sides of the cell are traversable */
		private static bool CanBeTraversedStraight (MazeCellWalls walls) { 
			return walls.AreOpenedEastwards && walls.AreOpenedWestwards
				|| walls.AreOpenedNorthwards && walls.AreOpenedSouthwards; 
		}

	}

	/** The shape of a cell when looked on it from above */
	[Serializable]
	public enum MazeCellShape {

		/**
		 *  .__.
		 *  |__|
		 *  '  '
		 */
		Trap,

		/** 
		 *  .__.
		 *  |  |
		 *  '  '
		 */
		DeadEnd,

		/** 
		 *  .  .
		 *  |  |
		 *  '  '
		 */
		Passage,

		/** 
		 *  .__.
		 *  |
		 *  '  '
		 */
		Corner,

		/**
		 *  .__.
		 *      
		 *  '  ' 
		 */
		Crossroad

	}

}
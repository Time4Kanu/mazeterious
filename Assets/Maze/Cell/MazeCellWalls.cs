﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	/** Contains all walls of this cell */
	[System.Serializable]
	public class MazeCellWalls {
		

		/** This cell's northern wall */
		public GameObject WallNorth;

		/** This cell's eastern wall */
		public GameObject WallEast;

		/** This cell's southern wall */
		public GameObject WallSouth;

		/** This cell's western wall */
		public GameObject WallWest;


		/** Checks, if these walls are opened to the north side */
		public bool AreOpenedNorthwards { 
			get { return WallNorth.activeSelf;	} 
		}

		/** Checks, if these walls are opened to the north side */
		public bool AreOpenedEastwards { 
			get { return WallEast.activeSelf; } 
		}

		/** Checks, if these walls are opened to the south side */
		public bool AreOpenedSouthwards { 
			get { return WallSouth.activeSelf; } 
		}

		/** Checks, if these walls are opened to the west side */
		public bool AreOpenedWestwards { 
			get { return WallWest.activeSelf; } 
		}


		/** The cell these walls belong to */
		private readonly MazeCell Cell;

		public MazeCellWalls (MazeCell cell) {
			Cell = cell;
		} 


		/** Removes the wall to the given cell */
		public void RemoveWallTo(MazeCell other) {
					
			// Remove the walls to and from the other cell
			Index thisIndex  = Cell .Location.Index;
			Index otherIndex = other.Location.Index;

			if (thisIndex.IsLeftFrom (otherIndex)) {
				this.WallEast.SetActive (false);
				other.Walls.WallWest.SetActive (false); 
			} else if(thisIndex.IsRightFrom(otherIndex)) {
				this.WallWest.SetActive (false);
				other.Walls.WallEast.SetActive (false);
			} else if(thisIndex.IsAbove (otherIndex)) {
				this.WallSouth.SetActive (false);
				other.Walls.WallNorth.SetActive (false);
			} else if(thisIndex.IsBelow(otherIndex)) {
				this.WallNorth.SetActive (false);
				other.Walls.WallSouth.SetActive (false);
			}

			// Register the other cell as reachable
			Cell .Location.ReachableCells.Add (other);
			other.Location.ReachableCells.Add (Cell);
		}

		/** Reactivates all four walls of this cell */
		public void Reset() {
			WallNorth.SetActive (true);
			WallEast .SetActive (true);
			WallSouth.SetActive (true);
			WallWest .SetActive (true);
		}

		/** Returns a random (active) wall of this cell's walls */
		public GameObject RandomWall (Seed seed) {
			return new List<GameObject> { WallNorth, WallEast, WallSouth, WallWest }
				.FindAll (wall => wall.activeSelf).RandomElement (seed);
		}

	}

}
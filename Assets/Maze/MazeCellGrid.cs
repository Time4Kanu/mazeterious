﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Mazeterious  {


	[Serializable]
	public class MazeCellGrid : IEnumerable<MazeCell> {


		[Serializable]
		public class Config {

			/** The cell prefab for creating new cells */
			public MazeCell CellPrefab;

			/** The (empty) parent object of all cells to create */
			public GameObject CellGrid;

		}


		/** If active, the cells have no ceiling */
		public BoolReactiveProperty Ceiling = new BoolReactiveProperty (false);
				  

		/** Contains all cells of this maze */
		[SerializeField] 
		private IndexMazeCellDictionary cells = new IndexMazeCellDictionary ();
		public ReactiveDictionary<Index, MazeCell> Cells { get { return cells.Dictionary; } }
		 
		/** Returns all cells of this maze */
		public IEnumerator<MazeCell> GetEnumerator () {
			return Cells.Values.GetEnumerator ();
		}

		/** Returns all cells of this maze */
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator () {
			return Cells.Values.GetEnumerator ();
		} 


		/** The configuration of this CellGrid */
		[SerializeField]
		public Config Configuration = new Config ();


		/** The maze's dimension */
		private readonly MazeDimension Dimension;

		/** Creates a new MazeCellGrid */
		public MazeCellGrid (MazeDimension dimension) {
			Dimension = dimension;
			Ceiling.SkipLatestValueOnSubscribe ().Subscribe (
				ceiling => Cells.Values.ForEach (cell => cell.SetCeiling (ceiling)));
		}

		 
		/** Recreates the maze based on the current values for Rows, Columns and Seed */
		public void Recreate () {
			RemoveAllCells ();  
			Dimension.PossibleIndices.Value.ForEach (CreateCellAt);
			Dimension.PossibleIndices.Value.ForEach (index => Cells[index].Location.AddNeighbours (this, Dimension));
		}

		/** Removes all cells from this maze */
		private void RemoveAllCells() {
			Cells.Clear (); 
			foreach (Transform cell in Configuration.CellGrid.transform) {
				cell.gameObject.RemoveFromEditor ();
			}
		}

		/** Creates a new cell at the given index */
		private void CreateCellAt (Index index) {
			MazeCell cell = (MazeCell) UnityEngine.Object.Instantiate (Configuration.CellPrefab, CellPosition (index), 
				Quaternion.identity, Configuration.CellGrid.transform);
			cell.Initialize (index, Ceiling.Value);
			Cells [index] = cell;
		}

		/** Calculates the local position of a cell at the given index */
		private Vector3 CellPosition (Index index) {
			Vector3 size = Configuration.CellPrefab.transform.localScale;
			return Configuration.CellGrid.transform.position + new Vector3 (size.x * index.X, 0, size.z * index.Y);
		}

	}

}

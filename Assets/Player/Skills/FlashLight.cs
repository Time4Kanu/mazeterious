﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;
using System.Collections;

public class FlashLight : MonoBehaviour {

    /** The InputManager giving access to the Flashlight Trigger */
    public InputManager Input;

	/** The outer circle of the flashlight */
	public GameObject Light;


	/** Registers this flashlight appropriately */
	private void Start () {
		Input.FlashLight.Trigger.Subscribe (Use);
	}
    
	/** Toggles the flashlight effect */
	protected void Use (KeyCode key) {
		Light.SetActive (!Light.activeSelf);
	}
		
}

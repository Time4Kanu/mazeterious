﻿using UnityEngine;
using UniRx;
using System.Collections;

public abstract class Skill : MonoBehaviour {


	/** The time this Skill will be on cooldown */
	public float CooldownTime = 0;


	/** Whether or not this skill currently is on cooldown */
	private bool IsOnCooldown;
		
	/** The duration this skill still will be on cooldown */
	private float CooldownTimeLeft = 0;


	/** Registers this skill appropriately */
	private void Start () {
		Command.Trigger.Subscribe (CheckUse);
	}

	/** Deregisters this skill appropriately */
	private void OnDestroy () {
		Command.Trigger.Subscribe (CheckUse);
	}


	/** Checks, if this skill is on cooldown. If not, starts a single usage */
	private void CheckUse (KeyCode key) {
		if (IsOnCooldown == false) {
			this.Use ();
			IsOnCooldown = true;
			CooldownTimeLeft = CooldownTime;
		}
	}


	/** Gets called each frame */
	private void Update () {
		if (this.IsOnCooldown) {
			CooldownTimeLeft -= Time.deltaTime;
			CooldownTimeLeft.IsNotPositive ().Then (EndCooldown);
			OnProgress (CooldownTimeLeft);
		}
	}

	/** Gets called at the end of the cooldown time */
	void EndCooldown () {
		IsOnCooldown = false;
		CooldownTimeLeft = 0;
		OnEndCooldown ();
	}


	/** Returns, whether or not the player currently tries to activate the skill (e.g. presses the corresponding key) */
	protected abstract InputCommand Command { get; }

	/** Gets called when the skill is not on cooldown and gets activated */
	protected abstract void Use ();

	/** Gets called during the duration of the cooldown to be able to always show the remaining duration */
	protected virtual void OnProgress (float timeRemaining) { }

	/** Gets called when the cooldown duration ends and UI may need to be resetted */
	protected virtual void OnEndCooldown () { }


}

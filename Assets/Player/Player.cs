﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	/** Whether or not the player currently is lit by a toplight */
	public bool IsLit = false;

	/** The player's flashlight */
	public GameObject Flashlight;

}

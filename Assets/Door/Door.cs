﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;

namespace Mazeterious {


	[RequireComponent(typeof(BoxCollider))]
	public class Door : MonoBehaviour {
		

		/** The event of a player touching this door */
		public Subject<GameObject> DoorTouched = new Subject<GameObject> ();


		/** The Maze to place this door into */
		public Maze Maze;

		/** The Keys object to check, if all keys have been collected */
		public Keys Keys;

		/** The Mesh of this door */
		public GameObject DoorMesh;

		/** The collider to check if a player enters the door */
		private BoxCollider Collider;


		/** Called at the beginning of the game */
		private void Start () {
			Keys.Collector.AllKeysCollected.Subscribe (SpawnDoor);
			DoorMesh.SetActive (false);
			Collider = GetComponent<BoxCollider> ();
			Collider.enabled = false;
			Maze.Positioner.RandomlyPositionDoor (Maze.Seeder, transform);
		}


		/** Spawns the door (mesh) and activates the collider */
		private void SpawnDoor<T> (T nil) {
			DoorMesh.SetActive (true);
			Collider.enabled = true;
		}

		/** Gets called, whenever a rigidbody collides with this door */
		private void OnTriggerEnter(Collider coll) {
			if (coll.CompareTag ("Player")) {
				DoorTouched.OnNext(coll.gameObject);
			}
		}

	}

}

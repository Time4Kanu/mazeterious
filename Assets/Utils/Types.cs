﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;
using System.Collections.Generic;


namespace Mazeterious {

	[Serializable] 
	public class IndexMazeCellDictionary : SerializableDictionary<Index, MazeCell> { };
	 
} 
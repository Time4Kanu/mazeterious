﻿using UnityEngine;
using UniRx;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace Mazeterious {

	[System.Serializable]
	public class GameObjectReactiveProperty : ReactiveProperty<GameObject> {

		public GameObjectReactiveProperty() : base() { }
		public GameObjectReactiveProperty(GameObject initialValue) : base(initialValue) { }
		public GameObjectReactiveProperty(IObservable<GameObject> observable) : base(observable) { }

	}

	[System.Serializable]
	public class IndexReactiveProperty : ReactiveProperty<Index> {

		public IndexReactiveProperty() : base() { }
		public IndexReactiveProperty(Index initialValue) : base(initialValue) { }
		public IndexReactiveProperty(IObservable<Index> observable) : base(observable) { }

	}

	[System.Serializable]
	public class IndexListReactiveProperty : ReactiveProperty<List<Index>> {
		 
		public IndexListReactiveProperty() : base() { }
		public IndexListReactiveProperty(List<Index> initialValue) : base(initialValue) { }
		public IndexListReactiveProperty(IObservable<List<Index>> observable) : base(observable) { }

	}

	public static class ReactivePropertyExtensions {

		public static GameObjectReactiveProperty ToReactiveProperty(this IObservable<GameObject> source) {
			return new GameObjectReactiveProperty (source);
		}

		public static IndexReactiveProperty ToReactiveProperty(this IObservable<Index> source) {
			return new IndexReactiveProperty (source);
		}

		public static IndexListReactiveProperty ToReactiveProperty(this IObservable<List<Index>> source) {
			return new IndexListReactiveProperty (source);
		}

	}

}
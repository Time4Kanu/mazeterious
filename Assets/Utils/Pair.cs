﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class Pair<A, B> {

	public readonly A a;
	public readonly B b;

	public Pair(A a, B b) {
		this.a = a;
		this.b = b;
	}


	public override int GetHashCode () {
		return hash (a) + 7 * hash(b);
	}

	private static int hash<C>(C c) {
		return c == null ? 0 : c.GetHashCode ();
	}


	public override bool Equals (object obj) {
		if(obj == null || !(obj is Pair<A,B>)) {
			return false;
		}
		Pair<A,B> other = obj as Pair<A,B>;
		return eq (a, other.a) && eq (b, other.b);
	}

	private static bool eq<C>(C c1, C c2) {
		return c1 == null && c2 == null 
			|| c1 != null && c2 != null && c1.Equals (c2);
	}


	public override string ToString () {
		return string.Format ("({0},{1})", str (a), str (b));
	}

	private static string str<C>(C c) {
		return c == null ? "null" : c.ToString ();
	}

}

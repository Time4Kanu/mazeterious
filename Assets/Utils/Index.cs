﻿using UnityEngine;
using System.Collections;
using System;

namespace Mazeterious {

	[Serializable]
	public struct Index {


		[SerializeField]
		private int x;
		public int X { get { return x; } }

		[SerializeField]
		private int y;
		public int Y { get { return y; } }


		public Index(int x, int y) { 
			this.x = x;
			this.y = y;
		}


		/** Adds the given amount to the X value */
		public Index PlusX(int dx) {
			return new Index (X + dx, Y);
		}

		/** Adds the given amount to the Y value */
		public Index PlusY(int dy) {
			return new Index (X, Y + dy);
		}

		/** Returns a new Index with the given X value */
		public Index WithX(int x) {
			return new Index (x, Y);
		}

		/** Returns a new Index with the given Y value */
		public Index WithY(int y) {
			return new Index (X, y);
		}


		/** Whether or not this Index has a lower X value than the other */
		public bool IsLeftFrom (Index other) {
			return this.X < other.X;
		}

		/** Whether or not this Index has a higher X value than the other */
		public bool IsRightFrom (Index other) {
			return this.X > other.X;
		}

		/** Whether or not this Index has a lower Y value than the other */
		public bool IsBelow (Index other) {
			return this.Y < other.Y;
		}

		/** Whether or not this Index has a higher Y value than the other */
		public bool IsAbove (Index other) {
			return this.Y > other.Y;
		}


		public override int GetHashCode () {
			return hash (X) + 7 * hash(Y);
		}

		private static int hash<C>(C c) {
			return c == null ? 0 : c.GetHashCode ();
		}


		public override string ToString () {
			return string.Format ("({0},{1})", str (X), str (Y));
		}

		private static string str<C>(C c) {
			return c == null ? "null" : c.ToString ();
		}

	}

}
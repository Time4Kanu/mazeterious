﻿using UnityEngine;
using UniRx;
using System.Collections;
using System.Collections.Generic;

namespace Mazeterious {

	[System.Serializable]
	public abstract class SerializableDictionary<K,V> : ISerializationCallbackReceiver
	{
		private System.Object MutexForDictionary = new System.Object();

		private ReactiveDictionary<K,V> dictionary = new ReactiveDictionary<K,V> ();
		public ReactiveDictionary<K,V> Dictionary { get { return dictionary; } }

		[SerializeField] private List<K> ListOfKeys = new List<K>();
		[SerializeField] private List<V> ListOfValues = new List<V>();


		public void OnBeforeSerialize()	{
			lock (MutexForDictionary) {
				ListOfKeys.Clear();
				ListOfValues.Clear();

				foreach (KeyValuePair<K,V> eachPair in dictionary)	{
					ListOfKeys.Add (eachPair.Key);
					ListOfValues.Add (eachPair.Value);
				}
			}
		}

		public void OnAfterDeserialize() {
			lock (MutexForDictionary) {
				dictionary.Clear();
				CheckIfKeyAndValueValid();

				for (int i = 0; i < ListOfKeys.Count; ++i)	{
					dictionary.Add (ListOfKeys[i], ListOfValues[i]);
				}
			}
		}

		private void CheckIfKeyAndValueValid() { 
			int numberOfKeys = ListOfKeys.Count;
			int numberOfValues = ListOfValues.Count;

			if (numberOfKeys != numberOfValues)	{
				throw new System.ArgumentException("(nKey, nValue) = ("
					+ numberOfKeys.ToString() + ", " 
					+ numberOfValues.ToString() + ") are NOT Equal!");
			}
		}

	}

}
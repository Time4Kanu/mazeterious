﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public static class Primitives {
	
	public static void Then (this bool value, Action action) {
		if (value) action ();	
	}

	public static void WhenNot (this bool value, Action action) {
		if (!value) action ();	
	}

	public static bool IsNegative (this float value) {
		return value < 0;
	}

	public static bool IsNotNegative (this float value) {
		return value >= 0;
	}

	public static bool IsZero (this float value) {
		return value == 0;
	}

	public static bool IsNotZero (this float value) {
		return value != 0;
	}

	public static bool IsPositive (this float value) {
		return value > 0;
	}

	public static bool IsNotPositive (this float value) {
		return value <= 0;
	}


	public static bool IsNegative (this int value) {
		return value < 0;
	}

	public static bool IsNotNegative (this int value) {
		return value >= 0;
	}

	public static bool IsZero (this int value) {
		return value == 0;
	}

	public static bool IsNotZero (this int value) {
		return value != 0;
	}

	public static bool IsPositive (this int value) {
		return value > 0;
	}

	public static bool IsNotPositive (this int value) {
		return value <= 0;
	}

	public static Vector3 WithX (this Vector3 vector, float x) {
		return new Vector3 (x, vector.y, vector.z);
	}

	public static Vector3 WithY (this Vector3 vector, float y) {
		return new Vector3 (vector.x, y, vector.z);
	}

	public static Vector3 WithZ (this Vector3 vector, float z) {
		return new Vector3 (vector.x, vector.y, z);
	}

	public static void ForEach<A> (this IEnumerable<A> enumerable, Action<A> action) {
		var enumerator = enumerable.GetEnumerator ();
		while (enumerator.MoveNext ()) {
			action (enumerator.Current);
		}
	}

	public static IEnumerable<int> Times (this int amount) {
		return Enumerable.Range (1, amount);
	}

	public static IEnumerable<A> Times<A> (this int amount, System.Func<int, A> func) {
		return Enumerable.Range (1, amount).Select (func);
	}

	public static void Times (this int amount, Action action) {
		Enumerable.Range (1, amount).ForEach (_ => action ());
	}

	public static void Times (this int amount, Action<int> action) {
		Enumerable.Range (1, amount).ForEach (i => action (i));
	}


	public static IEnumerable<A> Merge<A> (this IEnumerable<IEnumerable<A>> enumerable) {
		var result = new List<A> ();
		enumerable.ForEach (element => result.AddRange (element));
		return result;
	}

	public static IEnumerable<C> Zip<A,B,C> (this IEnumerable<A> first, IEnumerable<B> second, Function<A,B,C> zipper) {
		var result = new List<C> ();
		first.Zip (second, (a, b) => result.Add (zipper(a, b)));
		return result;
	}

	public static void Zip<A,B> (this IEnumerable<A> first, IEnumerable<B> second, Action<A,B> zipper) {
		var firstEnumerator = first.GetEnumerator ();
		var secondEnumerator = second.GetEnumerator ();
		while (firstEnumerator.MoveNext () && secondEnumerator.MoveNext ()) {
			zipper (firstEnumerator.Current, secondEnumerator.Current);
		}
	}

}
﻿using UnityEngine;
using System.Collections;

public struct Optional<A>
{
	private bool hasValue;
	private A a;

	public Optional(A value) {
		Debug.Assert (value != null);
		hasValue = true;
		a = value;
	}

	public static implicit operator Optional<A>(A a) {
		return a == null ? new Optional<A>() : new Optional<A> (a);
	}

	public void MaybeDo(Action<A> action) {
		if(hasValue) { action (a); }
	}

	public B Maybe<B>(Function<A, B> func, B @default) {
		return hasValue ? func (a) : @default;
	}

	public Optional<B> Map<B>(Function<A, B> selector) {
		return hasValue ? new Optional<B>(selector(a)) : new Optional<B>();
	}

	public Optional<B> Bind<B>(Function<A, Optional<B>> bind) {
		return hasValue ? bind(a) : new Optional<B>();
	}

	public bool Exists {
		get { return hasValue; }
	}

	public A GetOr(A @default) {
		return hasValue ? a : @default;
	}
}

public static class OptionalExtensions {

	public static void IfNotNull<A>(this A nullable, Action action) {
		if(nullable != null) {
			action ();
		}
	}

	public static void IfNotNull<A>(this A nullable, Action<A> action) {
		if(nullable != null) {
			action (nullable);
		}
	}

	public static void IfNotNull<A>(this A nullable, Action positive, Action negative) {
		if(nullable != null) {
			positive();
		} else {
			negative ();
		}
	}

	public static void IfNotNull<A>(this A nullable, Action<A> positive, Action negative) {
		if(nullable != null) {
			positive(nullable);
		} else {
			negative ();
		}
	}

	public static void IfNull<A>(this A nullable, Action action) {
		if(nullable == null) {
			action ();
		}
	}

	public static void If (this bool boolean, Action action) {
		if(boolean) {
			action ();
		}
	}

	public static void If (this GameObject gameObject, Action<GameObject> action) {
		if(gameObject) {
			action (gameObject);
		}
	}

	public static void IfNot (this bool boolean, Action action) {
		if(!boolean) {
			action ();
		}
	}

	public static void Execute (this Action action) {
		action.IfNotNull (a => a ());
	}

	public static void IfNotNull<A> (this Action<A> action, A arg) {
		action.IfNotNull (a => a (arg));
	}

}
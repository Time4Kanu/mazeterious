﻿using UnityEngine;
using System.Collections;

public delegate void Action();
public delegate void Action<A>(A a);
public delegate void Action<A, B>(A a, B b);
public delegate void Action<A, B, C>(A a, B b, C c);

public delegate A Function<A>();
public delegate B Function<A, B>(A a);
public delegate C Function<A, B, C>(A a, B b);
public delegate D Function<A, B, C, D>(A a, B b, C c);

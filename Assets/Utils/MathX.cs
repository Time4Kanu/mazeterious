﻿using UnityEngine;
using System.Collections;

public static class MathX {

	public static float RoundToPrecision(this float f, int digits) {
		float basis = Mathf.Pow (10F, digits);
		return Mathf.Round (f * basis) / basis;
	}

}

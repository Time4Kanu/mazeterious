﻿using UnityEngine;
using UniRx;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public struct Seed {

	public int Value;

	public Seed (int value) {
		Value = value;
	}

	private System.Random Random () {
		return new System.Random (Value);
	}


	public void ForEach<A> (IEnumerable<A> enumerable, Action<Seed, A> action) {
		System.Random random = Random ();
		enumerable.ForEach (element => action (new Seed (random.Next ()), element));
	}

	public void Times (int amount, Action<Seed, int> action) {
		ForEach(Enumerable.Range (1, amount), action);
	}

	public A RandomElement<A> (IEnumerable<A> collection) {
		Debug.Assert (collection.Count () > 0, "Cannot extract random element of an empty collection");
		return collection.ElementAt (Random ().Next (collection.Count ()));
	}

	public List<A> Shuffle<A> (IEnumerable<A> enumerable) {  
		System.Random random = Random ();
		var list = new List<A> (enumerable);
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = Mathf.RoundToInt(random.Next (n + 1));  
			A value = list [k];
			list[k] = list[n];  
			list[n] = value;  
		}  
		return list;
	}

	public Seed RandomSeed () {
		return new Seed (Random ().Next ());
	}

	public float RandomFloat () {
		return (float) Random ().NextDouble (); 
	}

	public float RandomFloat (float min, float max) {
		return min + (max - min) * RandomFloat ();
	}

}

public static class SeedExtensions {

	public static A RandomElement<A> (this IEnumerable<A> collection, Seed seed) {
		return seed.RandomElement (collection);
	}

	public static void ForEach<A> (this IEnumerable<A> enumerable, Seed seed, Action<Seed, A> action) {
		seed.ForEach (enumerable, action);
	}

	public static List<A> Shuffle<A> (this IEnumerable<A> enumerable, Seed seed) {  
		return seed.Shuffle (enumerable);
	}

	public static Seed AsSeed (this ReactiveProperty<int> property) {
		return new Seed (property.Value);
	}

}
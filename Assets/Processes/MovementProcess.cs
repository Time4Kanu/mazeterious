﻿using UnityEngine;
using System.Collections;
using System;

public class MovementProcess : MonoBehaviour {


	/** The object to move */
	public Transform Object;

	/** The point in the world where to move the object */
	public Vector3 TargetLocation;

	/** The speed in which the object should move */
	public float MovementSpeed;


	/** The Action to start when the movement is finished */
	private Optional<Action> OnFinish;

	/** The Ticker of this movement process to be able to interrupt it */
	private Optional<Metronome.TickerWithDuration> Ticker;


	/** Gets called on instantiating this movement process */
	void Start() {
		Vector3 way = TargetLocation - Object.position;
		float timeNeeded = way.magnitude / MovementSpeed;
		Ticker = Metronome.Register(timeNeeded, Tick, Finish);
	}


	/** Sets the nesseccary action to be called when this movement process ends */
	public void SetOnFinish(Action onFinish) {
		OnFinish = onFinish;
	}

	/** Stops the movement of the object and destroys this movement process */
	public void Interrupt() {
		Ticker.MaybeDo (t => t.Interrupt ());
		Destroy (this.gameObject);
	}
	
	/** Gets called once each tick */
	private void Tick (float deltaTime) {
		Object.IfNotNull (() => {
			Vector3 direction = TargetLocation - Object.position;
			Vector3 movement = direction.normalized * MovementSpeed * deltaTime;
			Object.position += (movement);
		});
	}

	/** Gets called at the end of the movement process */
	private void Finish() {
		Object.position = TargetLocation;
		OnFinish.MaybeDo (f => f());
		Destroy (this.gameObject);
	}

}

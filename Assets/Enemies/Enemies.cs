﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Mazeterious {

    public class Enemies : MonoBehaviour {

        [System.Serializable]
        public class Config {

            /** The maze to spawn the enemies in */
            public Maze Maze;

            /** The enemy prefab to spawn */
            public Enemy EnemyPrefab;
    
            /** The parent gameobject of all enemies to spawn */
            public GameObject EnemyHolder;

            /** The player to be detected by the enemies */
            public Player Player;

        }


        /** The event of all enemies being respawned */
        public Subject<List<Enemy>> EnemiesRespawned = new Subject<List<Enemy>> ();
        
        
        /** The total amount of enemies in this Maze */
        [RangeReactiveProperty(0, 10)]
        public IntReactiveProperty AmountOfEnemies = new IntReactiveProperty(1);

        /** A list of all enemies in the maze */
        public List<Enemy> AllEnemies;

        public Config Configuration;


        /** Creating a new Enemies script */
        private Enemies () {
            AmountOfEnemies.SkipLatestValueOnSubscribe ().DistinctUntilChanged ().Subscribe (RespawnEnemies);
            Maze.Recreation.Subscribe (RespawnEnemies);
        }

        /** Called at the beginning of the game */
        private void Start () {
            AmountOfEnemies.Value = SceneTransport.NumberOfEnemies ?? AmountOfEnemies.Value;
            RespawnEnemies (Unit.Default);
        }

        /** Respawns all enemies of this maze */
        private void RespawnEnemies<T> (T nil) {
            EnsureValidObjects ();
            RemoveAllEnemies ();
            AmountOfEnemies.Value.Times (SpawnEnemy);
            Configuration.Maze.Positioner.RandomlyPositionActors (Configuration.Maze.Seeder,
                AllEnemies.Select (enemy => enemy.transform));
            EnemiesRespawned.OnNext (AllEnemies);
        }

        /** Ensures, that all object references used are still valid */
        private void EnsureValidObjects () {
            Configuration.Maze = GameObject.Find ("Maze").GetComponent<Maze> ();
        }

        /** Removes all Enemies associated to this Enemies object */
        private void RemoveAllEnemies () {
            AllEnemies.Clear ();
            foreach (Transform enemy in Configuration.EnemyHolder.transform) {
                enemy.gameObject.RemoveFromEditor ();
            }
        }

        /** Spawns a new enemy */
        private void SpawnEnemy (int number) {
            Enemy enemy = (Enemy) GameObject.Instantiate (Configuration.EnemyPrefab,
                Configuration.EnemyPrefab.transform.position, Quaternion.identity, Configuration.EnemyHolder.transform);
            enemy.Initialize (number, Configuration.Maze, Configuration.Player);
            AllEnemies.Add (enemy);
        }

    }

}
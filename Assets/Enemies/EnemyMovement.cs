﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;


namespace Mazeterious {

	[RequireComponent(typeof(Seeker))]
	public class EnemyMovement : MonoBehaviour {


		public enum MovementState {

			/** The enemy is calculating his next path */
			CALCULATING,

			/** The enemy is turning towards his path's first waypoint */
			TURNING,

			/** The enemy is moving along his path */
			MOVING

		}


		/** The seeder to define which paths to take */
		public Seeder Seeder;

		/** The speed of the enemy's movement */
		public float MovementSpeed;

		/** The maximum rotation speed of the enemy when moving around corners */
		public float RotationSpeedMoving = 20;

		/** The maximum rotation speed of the enemy when turning to his path's initial waypoint */
		public float RotationSpeedTurning = 50;

		/** The maze to calculate the next target cell from */
		public Maze Maze;


		/** The seeker for finding the path to the next target */
		private Seeker Seeker;

		/** The enemy's transform */
		private Transform Enemy;

		/** An ordered list containing the next waypoints to move to */
		private List<Vector3> Path = new List<Vector3> ();

		/** The minimum distance from the current target waypoint to skip it and continue with the next waypoint */
		private float WaypointChangeDistance = 1;

		/** The state of this movement script */
		public MovementState State = MovementState.CALCULATING;


		/** Called at the beginning of the game */
		private void Start () {
			Enemy = transform;
			Seeker = GetComponent<Seeker> ();
			RecalculatePath ();
		}

		/** Recalculates a new path to a random target cell */
		private void RecalculatePath () {
			Path.Clear ();
			var target = Maze.Positioner.RandomMazeCell (Seeder.RenewSeed ()).transform.position;
			AstarPath.active.Scan ();
			Seeker.StartPath (transform.position, target, PathCalculated);
			State = MovementState.CALCULATING;
		}

		/** To be called when the path calculation finished */
		private void PathCalculated (Path path) {					
			if (path.error) {
				Debug.LogError ("No path found!");
				RecalculatePath ();
			} else {
				Path = path.vectorPath;
				Path.RemoveAt (0);
				State = MovementState.TURNING;
			}
		}

		/** Called each frame */
		private void Update () {
			EnsureNextWaypointExists ();

			switch (State) {

			case MovementState.CALCULATING:
				// do nothing
				break;

			case MovementState.TURNING:
				TurnToNextWaypoint ();
				break;

			case MovementState.MOVING:
				MoveOnPath ();
				break;

			}
		}

		/** Turns slowly to the next waypoint */
		private void TurnToNextWaypoint () {
			var toNextWaypoint = ToNextWaypoint ();
			SlowlyRotateTowards (toNextWaypoint, RotationSpeedTurning);
			State = Vector3.Angle (Enemy.forward, toNextWaypoint) < 10 ? MovementState.MOVING : MovementState.TURNING;
		}

		/** Returns the direction to the next waypoint */
		private Vector3 ToNextWaypoint () {
			return Path.Count == 0 ? Enemy.forward : Path.First () - Enemy.position;
		}

		/** Moves on to the next waypoint */
		private void MoveOnPath () {			
			EnsureNextWaypointIsUpToDate ();
			var toNextWaypoint = ToNextWaypoint ();
			Enemy.position = Enemy.position + toNextWaypoint.normalized * MovementSpeed * Time.deltaTime;
			SlowlyRotateTowards (toNextWaypoint, RotationSpeedMoving);
		}

		/** Slowly rotates the enemy towards the given target direction */
		private void SlowlyRotateTowards (Vector3 direction, float rotationSpeed) {
			var targetRotation = direction == Vector3.zero ? Enemy.rotation : Quaternion.LookRotation (direction);
			Enemy.rotation = Quaternion.RotateTowards (Enemy.rotation, targetRotation, rotationSpeed * Time.deltaTime);
		}

		/** If necessary, recalculates the path */
		private void EnsureNextWaypointExists () {
			if (State != MovementState.CALCULATING && Path.Count == 0) {
				RecalculatePath ();
			}
		}

		/** If necessary, recalculates the next waypoint of the path */
		private void EnsureNextWaypointIsUpToDate () {
			if (Vector3.Distance (transform.position, Path.First ()) <= WaypointChangeDistance) {
				Path.RemoveAt (0);	
			}
		}

	}

}
﻿using UnityEngine;
using UniRx;
using System.Collections;

namespace Mazeterious {

	public class EnemyDetection : MonoBehaviour {


		/** The whole viewing angle of the enemy's sight */
		public FloatReactiveProperty ViewAngle = new FloatReactiveProperty (70);

		/** The range of the enemy's sight */
		public FloatReactiveProperty ViewRange = new FloatReactiveProperty (15);

		/** The viewing angle for spotting a lit player */
		public FloatReactiveProperty ViewAngleLit = new FloatReactiveProperty (100);

		/** If the player deceeds this distance to the enemy, he will be detected, independent of sight */
		public FloatReactiveProperty PresenceFeelingDistance = new FloatReactiveProperty (2);

		/** Whether or not the game shall end when the player is spotted */
		public BoolReactiveProperty EndGameOnDetection = new BoolReactiveProperty (false);


		[System.Serializable]
		public class Config {

			/** The EyeMovement script of this enemy */
			public EnemyEyeMovement EyeMovement;

			/** The alarm light to show if the enemy is detecting the player */
			public GameObject AlarmLight;

			/** The flashlight of the enemy to represent the enemy's sight */
			public Light EnemyFlashLight;

			/** The player to detect */
			public Player Player;

		}

		public Config Configuration;


		/** This enemy's transform */
		private Transform Enemy;

		/** The layer mask of maze objects */
		private int MazeLayerMask;


		/** The event of a certain enemy spotting the player */
		public Subject<EnemyDetection> PlayerSpotted = new Subject<EnemyDetection> ();


		/** Creates a new EnemyDetection script */
		public EnemyDetection () {
			ViewAngle.SkipLatestValueOnSubscribe ().Subscribe (angle => Configuration.EnemyFlashLight.spotAngle = angle);
			ViewRange.SkipLatestValueOnSubscribe ().Subscribe (range => Configuration.EnemyFlashLight.range = range);
		}


		/** Called at the beginning of the game */
		void Start () {
			Enemy = transform;
			MazeLayerMask = LayerMask.GetMask ("Maze");

			#if !UNITY_EDITOR
			EndGameOnDetection.Value = true;
			#endif
		}

        /** Called at the end of the scene */
        void OnDestroy () {
            PlayerSpotted.Dispose ();
            PlayerSpotted = new Subject<EnemyDetection> ();
        }
		
		/** Called once per frame */
		void Update () {
			var playerSpotted = SpotsPlayer ();
			Configuration.AlarmLight.SetActive (playerSpotted);

			if (playerSpotted && EndGameOnDetection.Value) {
				var eyes = Configuration.EyeMovement.Configuration.Focus;
				Configuration.Player.transform.LookAt (eyes.position);
				PlayerSpotted.OnNext (this);
			} 
		}

		/** Calculates, if the enemy is able to spot the player */
		private bool SpotsPlayer () {
			return PlayerIsInLineOfSight () && (
				PlayerIsTooClose () || 
				PlayerIsInViewingRange () && PlayerIsInViewAngle ());
		}

		/** Calculates, if the player got too close to the enemy to stay invisible */
		private bool PlayerIsTooClose () {
			return Vector3.Distance (Enemy.position, Configuration.Player.transform.position) 
								<= PresenceFeelingDistance.Value;
		}

		/** Calculates, if the player is in viewing range or has his flashlight active */
		private bool PlayerIsInViewingRange () {
			var playerDistance = Vector3.Distance (Enemy.position, Configuration.Player.transform.position);
			return PlayerIsLit || playerDistance <= ViewRange.Value;
		}

		/** Calculates, if the player is in the viewing angle of the enemy */
		private bool PlayerIsInViewAngle () {
			var viewDirection = Focus.forward;
			var toPlayer = Configuration.Player.transform.position - Focus.position;
			var viewAngle = PlayerIsLit ? ViewAngleLit.Value : ViewAngle.Value;
			return Vector3.Angle (viewDirection, toPlayer) * 2 < viewAngle;
		}

		/** Calculates, if the enemy has free line of sight to the player */
		private bool PlayerIsInLineOfSight () {
			return PlayerCanBeSeenFrom (LeftEye) || PlayerCanBeSeenFrom (RightEye);
		}

		/** Calculates, if a certain transform has free line of sight to the player */
		private bool PlayerCanBeSeenFrom (Transform eye) {
			var toPlayer = new Ray (eye.position, Configuration.Player.transform.position - eye.position);
			var mazeHit = new RaycastHit ();
			Physics.Raycast (toPlayer, out mazeHit, float.MaxValue, MazeLayerMask);

			var enemyToPlayer = Vector3.Distance (Enemy.position, Configuration.Player.transform.position);
			var enemyToMaze   = Vector3.Distance (Enemy.position, mazeHit.point);
			return enemyToPlayer <= enemyToMaze;
		}

		/** The Focus of this enemy's eye movement */
		private Transform Focus {
			get { return Configuration.EyeMovement.Configuration.Focus; }
		}

		/** This enemy's left eye */
		private Transform LeftEye {
			get { return Configuration.EyeMovement.Configuration.LeftEye; }
		}

		/** This enemy's right eye */
		private Transform RightEye {
			get { return Configuration.EyeMovement.Configuration.RightEye; }
		}

		/** Wheather or not the player's flashlight is active or he is below a top light */
		private bool PlayerIsLit {
			get { return Configuration.Player.Flashlight.activeSelf || Configuration.Player.IsLit; }
		}

	}

}
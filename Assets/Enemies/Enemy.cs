﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mazeterious {

    public class Enemy : MonoBehaviour {

        /** The detection script of this enemy */
        public EnemyDetection Detection;

        /** The eye movement script of this enemy */
        public EnemyEyeMovement Eyes;

        /** The movement script of this enemy */
        public EnemyMovement Movement;

        /** Initializes this Enemy */
        public void Initialize (int number, Maze maze, Player player) {
            name = "Enemy " + number;
            Detection.Configuration.Player = player;
            Movement.Maze = maze;
        }

    }

}
﻿using UnityEngine;
using System.Collections;

namespace Mazeterious {

	public class EnemyEyeMovement : MonoBehaviour {


		[System.Serializable]
		public class HorizontalConfig {

			/** The viewing range to move the focus horizontally. 90° means 45° to each side */
			public float Angle = 90;

			/** The speed in which to rotate between left and right */
			public float Speed = 5;

		}

		public HorizontalConfig Horizontal;


		[System.Serializable]
		public class VerticalConfig {

			/** The minimum angle to move the focus downwards */
			public float MinimumAngle = -20; 

			/** The maximum angle to move the focus updwards */
			public float MaximumAngle = 5;

			/** The speed in which to rotate between up and down */
			public float Speed = 5;

		}

		public VerticalConfig Vertical;


		[System.Serializable]
		public class Config {

			/** The distance of the focus point the eyes will look at */
			public float ViewRange = 10;

			/** The focus aiming in the direction of the current sight */
			public Transform Focus;

			/** The left eye to follow the focus */
			public Transform LeftEye;

			/** The right eye to follow the focus */
			public Transform RightEye;

			/** The Flashlight of the Enemy */
			public Transform FlashLight;

		}

		public Config Configuration;


		/** The difference to add to the horizontal viewing angle per tick */
		private float DifferenceX;

		/** The maximum angle for viewing to the left */
		private float MinAngleX;

		/** The maximum angle for viewing to the right */
		private float MaxAngleX;


		/** The difference to add to the vertical viewing angle per tick */
		private float DifferenceY;

		/** The maximum angle for viewing to the bottom */
		private float MinAngleY;

		/** The maximum angle for viewing to the top */
		private float MaxAngleY;


		/** Called at the beginning of the game */
		void Start () {
			DifferenceX = Horizontal.Speed;
			MinAngleX  = CurrentAngleX - Horizontal.Angle / 2;
			MaxAngleX = CurrentAngleX + Horizontal.Angle / 2;

			DifferenceY = Vertical.Speed;
			MinAngleY = CurrentAngleY - Vertical.MaximumAngle;
			MaxAngleY = CurrentAngleY - Vertical.MinimumAngle;
		}
		
		/** Called once per frame */
		void Update () {
			EnsureTargetFocus ();
			MoveFocus ();
			AdjustEyesAndFlashlight ();
		}

		/** Moves the Focus a certain amount in the moving direction */
		private void MoveFocus () {
			CurrentAngleX = Mathf.Clamp(CurrentAngleX + DifferenceX * Time.deltaTime, MinAngleX, MaxAngleX);
			CurrentAngleY = Mathf.Clamp(CurrentAngleY + DifferenceY * Time.deltaTime, MinAngleY, MaxAngleY);
		}

		/** Ensures, that a new Target Focus is created after reaching the last */
		private void EnsureTargetFocus () {
			if (CurrentAngleX <= MinAngleX || CurrentAngleX >= MaxAngleX) {
				DifferenceX = -DifferenceX;
			}
			if (CurrentAngleY <= MinAngleY || CurrentAngleY >= MaxAngleY) {
				DifferenceY = -DifferenceY;
			}
		}

		/** The current, horizontal angle, represented by the current y angle rotated by 180 degrees */
		private float CurrentAngleX {
			get { return (Configuration.Focus.localEulerAngles.y + 180) % 360; }
			set { Configuration.Focus.localRotation = Quaternion.Euler (Configuration.Focus.localEulerAngles.WithY (value - 180)); }
		}

		/** The current, vertical angle, represented by the current x angle rotated by 180 degrees */
		private float CurrentAngleY {
			get { return (Configuration.Focus.localEulerAngles.x + 180) % 360; }
			set { Configuration.Focus.localRotation = Quaternion.Euler (Configuration.Focus.localEulerAngles.WithX (value - 180)); }
		}

		/** Adjusts the eyes to look at the same point in the world */
		private void AdjustEyesAndFlashlight () {
			var focusPoint = Configuration.Focus.position + Configuration.Focus.forward * Configuration.ViewRange;
			Configuration.LeftEye.rotation = Quaternion.LookRotation (focusPoint - Configuration.LeftEye.position);
			Configuration.RightEye.rotation = Quaternion.LookRotation (focusPoint - Configuration.RightEye.position);
			Configuration.FlashLight.rotation = Quaternion.LookRotation (focusPoint - Configuration.FlashLight.position);
		}

	}

}